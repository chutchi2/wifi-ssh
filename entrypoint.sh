#!/bin/bash
#The MIT License 
# 
#Permission is hereby granted, free of charge, to any person obtaining a copy 
#of this software and associated documentation files (the "Software"), to deal 
#in the Software without restriction, including without limitation the rights 
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
#copies of the Software, and to permit persons to whom the Software is 
#furnished to do so, subject to the following conditions: 
# 
#The above copyright notice and this permission notice shall be included in 
#all copies or substantial portions of the Software. 

echo "Generating SSH keypair..."
mkdir -p /mnt/USB_PACKAGE/ssh
ssh-keygen -b 2048 -t rsa -f /mnt/id_rsa -q -N ""
mv /mnt/id_rsa.pub /mnt/USB_PACKAGE/ssh

config_wifi()
{   
    ssid="$1"
    psk="$2"

    exec 3>&1
    config_w=$(dialog --ok-label "Ok" \
            --backtitle "Setting up SSH payload for main_loop" \
            --title "Wifi settings config" \
            --form "Specify your wifi's settings \n(Forward slash [/] not allowed in vaues!)" \
              15 60 0 \
            "SSID:"      1 1	    "$ssid"      1 10 30 0 \
            "PSK: "      2 1	    "$psk"      2 10 30 0 \
            2>&1 1>&3 | 
            {
                read -r ssid
                read -r psk    
                echo "Passing SSID"
                sed -i.bak "s/<REPLACEWITHSSID>/$ssid/g" /ctx/template_code.sh
                echo "Passing PSK"
                sed -i.bak "s/<REPLACEWITHPSK>/$psk/g" /ctx/template_code.sh
            })
    exec 3>&-
} 

config_wifi "WIFINAME" "WIFIPASSWORD"
cp /ctx/template_code.sh /mnt/USB_PACKAGE/main_loop_code_ssh.sh
cp /ctx/template_loop.sh /mnt/USB_PACKAGE/main_loop.sh
