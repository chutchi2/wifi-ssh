# Wifi + SSH

This project contains the code needed to enable Wifi and SSH in your unit via main_loop

# How to build the image?

Go to the folder of this project and run on the terminal
```
docker build -t gen5wsshloop -f ./dockerfile ./
```

# How to patch the update?

Go to the folder where you have the downloaded and run the following: 
```
docker run -it -v $PWD:/mnt gen5wsshloop
```
And just follow the prompts.  You will end up with a folder named `USB_PACKAGE`; you copy its contents to the root of your USB