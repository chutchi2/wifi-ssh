#!/bin/bash

USB_PATH=$(dirname $0)
cd $USB_PATH

LOOP_OUTPUT=./loop_output_ssh.txt
LOOP_LOCK=./dev_loop_running_lock_ssh
LOOP_SHELL_SCRIPT=./main_loop_code_ssh.sh
printf "\n----- [LOOP_START] Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) [LOOP_START] -----\n" >> $LOOP_OUTPUT ;
if test -f "$LOOP_LOCK"; then
    printf "Loop lock file [$LOOP_LOCK] found. Remove it if you intended to run and no instance is running." >> $LOOP_OUTPUT ;
    exit 1;
fi

while true
do
    touch $LOOP_LOCK
    if test -f "$LOOP_SHELL_SCRIPT"; then
        printf "\n\n\n--- [LOOP_EXECUTION_START] Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) [LOOP_EXECUTION_START] --- \n" >> $LOOP_OUTPUT ;
        bash $LOOP_SHELL_SCRIPT >> $LOOP_OUTPUT 2>&1 ;
        printf "\n--- [LOOP_EXECUTION_END] Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) [LOOP_EXECUTION_END] --- \n\n\n" >> $LOOP_OUTPUT ;
    fi
    sleep 10
done
