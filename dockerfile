FROM alpine
RUN apk add wpa_supplicant openssh-keygen bash dialog
COPY ./ /ctx
RUN chmod +x /ctx/entrypoint.sh
ENTRYPOINT ["/ctx/entrypoint.sh"]
