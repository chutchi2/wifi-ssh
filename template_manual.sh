#!/bin/bash

# Changes done here are lost after reboot;
# Thanks Javi for making this possible!


USB_PATH=$(dirname $0)
cd $USB_PATH

printf "\n----- Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) -----\n" ;


# shut down all networking + iptables, this should disable cell data and prevent abuse
systemctl stop supplicantmanager ;
systemctl stop iptables-manager ;
systemctl stop connman ;

# connect to wifi
iptables -P INPUT ACCEPT ;
wpa_passphrase YOUR_SSID "YOUR_PSK" > /tmp/wpa.conf ;
wpa_supplicant -B -i wlan0 -c /tmp/wpa.conf > /tmp/wpa_supplicant.log 2>1 &


### IP ROUTING CONFIG ###
## setup IP address according to your LAN, default gw left as exercise

# ip addr add 192.168.1.1 dev wlan0
# ip route add 192.168.1.0/24 dev wlan0

# ip addr add 192.168.0.1 dev wlan0
# ip route add 192.168.0.0/24 dev wlan0

ip addr add 192.168.1.254 dev wlan0 ;
ip route add 192.168.1.0/24 dev wlan0 ;
### IP ROUTING CONFIG ###

# setup dropbear
mkdir -p /tmp/dropbear ;
dropbearkey -t rsa -f /tmp/dropbear/dropbear_rsa_host_key ;
mkdir -p /home/root/.ssh ;
chmod 700 /home/root/.ssh ;

#### PUT YOUR authorized_keys in /home/root/.ssh/authorized_keys

chmod 600 /home/root/.ssh/authorized_keys ; 

# dropbear will check this file for valid shells before auth
echo /bin/sh >> /tmp/shells ;

# run dropbear on port 2002, no fork, send logs to stdout, disallow pw login, etc. Sending to BG with &
dropbear -F -E -p 2002 -s -g -r /tmp/dropbear/dropbear_rsa_host_key &


printf "\n----- END Caller:($(ps -o comm= $PPID)) - PID:($PPID) - At($(date)) END -----\n" ;
